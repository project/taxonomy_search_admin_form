<?php

namespace Drupal\taxonomy_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Edit search form for taxonomy term.
 */
class EditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy_search';
  }

  /**
   * Build taxonomy search form.
   *
   * @param array $form
   *   Original form content.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   *
   * @return array
   *   Form array containing data provided by the client.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['tid'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'taxonomy_term',
      '#title' => $this->t('Term'),
      '#description' => $this->t('Which term you are looking for?'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#title' => $this->t('Search'),
      '#default_value' => "Search",
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $tid = $form_state->getValue('tid');
    $response = Url::fromRoute('entity.taxonomy_term.edit_form', ['taxonomy_term' => $tid]);
    $form_state->setRedirectUrl($response);
  }

}
