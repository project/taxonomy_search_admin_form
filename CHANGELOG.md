# Changelog

## [1.1.0] - 2023-10-06
### Changed
- Add autocomplete for terms search
- Fix PHPCS errors

## [1.1.0] - 2023-10-05
### Added
- CHANGELOG.md
