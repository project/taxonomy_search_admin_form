# Taxonomy admin search form

Taxonomy search module provides a search form for taxonomy terms

For a full description of the module, visit the
[project page](https://www.drupal.org/project/taxonomy_search_admin_form).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/taxonomy_search_admin_form).

## Table of contents

Requirements  
Installation  
Maintainers  

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Maintainers

Raphael COLBOC - [racol](https://www.drupal.org/u/racol)

The drupal module taxonomy_search was developed by Raphael Colboc (drupalname:racol).
